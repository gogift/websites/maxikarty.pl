
/**
 * Namespace with a unified "to console"-system
 */
Debug = function()
{
	var logLevel = 0
	
	/**
	 * Set log level
	 * 0 = off
	 * 1 = trace
	 * 2 = debug
	 *
	 * @param int The new log level
	 * @return void
	 */
	function setLogLevel(n)
	{
		logLevel = n
	}
	
	/**
	 * Send trace data
	 *
	 * @param string Where are we
	 * @return void
	 */
	function trace(data)
	{
		if (logLevel >= 1)
		{
			worker(data)
		}
	}
	
	/**
	 * Send debug data
	 *
	 * @param object The data to log
	 * @return void
	 */
	function debug(data)
	{
		if (logLevel >= 2)
		{
			worker(data)
		}
	}
	
	/**
	 * Private method that performs
	 * the actual call to the browser's
	 * console
	 *
	 * @param object The data to log
	 * @return void
	 */
	function worker(data)
	{
		if (typeof(console) != typeof(undefined))
		{
			console.log(data)
			return
		}
		
		else if (typeof(opera) != typeof(undefined))
		{
			opera.postError(data)
			return
		}
	}
	
	return {
		"setLogLevel" : setLogLevel,
		"t" : trace,
		"d" : debug,
		"trace" : trace,
		"debug" : debug
	}
}()
