
/**
 * Methods for working with the Viewport
 *
 * From: http://geekswithblogs.net/khillinger/archive/2009/06/04/jquery-and-the-viewport-dimensions.aspx
 */
window.Viewport = 
{
	height: function() { 
		return $(window).height(); 
	},

	width: function() {
		return $(window).width();
	},

	scrollTop: function() {
		return $(window).scrollTop();
	},

	scrollLeft: function() {
		return $(window).scrollLeft();
	},

	centerElement: function($elm) {
		
		$elm.css('top', '0')
		
		if (Viewport.height() <= $elm.height())
		{
			$elm.css('position', 'absolute').css('top', Viewport.scrollTop())
		}
		else
		{
			$elm.css('position', 'fixed')
			$elm.css('top', (Viewport.height()-$elm.height())/2)
		}
		
		if (Viewport.width() > $elm.width())
		{
			$elm.css('left', (Viewport.width()-$elm.width())/2)
		}
		else
		{
			$elm.css('left', 0)
		}
	},
	
	elementAtElement: function($elm, $otherElm, xOffset, yOffset) {
	
		var offset = $otherElm.offset();
		
		$elm.css('top', offset.top + yOffset)
		$elm.css('left', offset.left + xOffset)
	}
};